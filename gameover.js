var gameover = {
    preload: function() {
        game.load.image('background', 'asset/sky.png');
        game.load.image('button4', 'asset/button4.png');
        game.load.image('button5', 'asset/button5.png');
        game.load.image('button6', 'asset/button6.png');
    },
    create: function() {
        game.stage.backgroundColor = '#B045FF';
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'background');

        if(win) this.msg = '你活著了';
        else this.msg = '你死了'

        this.gamename = game.add.text(game.width/2, game.height/2-200, this.msg, { font: '70px DFKai-sb', fill: '#000' });
        this.gamename.anchor.setTo(0.5,0.5);

        this.score1 = game.add.text(game.width/2, game.height/2-100, `分數: ${score}分`, { font: '70px DFKai-sb', fill: '#000' });
        this.score1.anchor.setTo(0.5,0.5);

        if(this.msg == '你活著了' && level<=3){
            this.button6 = game.add.button(game.width/2, game.height/2, 'button6', this.nextlevel, this, 2, 1, 0);
            this.button6.anchor.setTo(0.5,0.5);
            this.button6.scale.setTo(0.3, 0.3);
        }
        else { 
            this.button5 = game.add.button(game.width/2, game.height/2, 'button5', this.startgame, this, 2, 1, 0);
            this.button5.anchor.setTo(0.5,0.5);
            this.button5.scale.setTo(0.3, 0.3);
        }

        this.button4 = game.add.button(game.width/2, game.height/2+150, 'button4', this.backmenu, this, 2, 1, 0);
        this.button4.anchor.setTo(0.5,0.5);
        this.button4.scale.setTo(0.3, 0.3);



    },

    update: function() {

    },
    
    nextlevel: function(){
        game.state.start(`level${level}`);
    },

    startgame: function(){
        game.state.start('level1');
    },

    backmenu: function(){
        game.state.start('menu');
    }
};