var levelselect = {
    preload: function() {
        game.load.image('background', 'asset/sky.png');
        game.load.spritesheet('button1', 'asset/bird.png', 62, 46);
        game.load.spritesheet('button2', 'asset/bird1.png', 124, 67);
        game.load.spritesheet('button3', 'asset/boss.png', 165, 179);
    },
    create: function() {
        game.stage.backgroundColor = '#B045FF';
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'background');

        this.gamename = game.add.text(game.width/2, game.height/2-100, '關卡選擇', { font: '100px DFKai-sb', fill: '#000' });
        this.gamename.anchor.setTo(0.5,0.5);

        this.button1 = game.add.button(game.width/2-200, game.height/2+100, 'button1', this.level1, this, 2, 1, 0);
        this.button1.anchor.setTo(0.5,0.5);
        this.button1.scale.setTo(2.5, 2.5);


        this.button2 = game.add.button(game.width/2, game.height/2+100, 'button2', this.level2, this, 2, 1, 0);
        this.button2.anchor.setTo(0.5,0.5);
        this.button2.scale.setTo(1.2, 1.2);

        this.button3 = game.add.button(game.width/2+200, game.height/2+100, 'button3', this.level3, this, 2, 1, 0);
        this.button3.anchor.setTo(0.5,0.5);
        this.button3.scale.setTo(0.8, 0.8);
    },

    update: function() {

    },

    level1: function(){
        game.state.start('level1');
    },

    level2: function(){
        game.state.start('level2');
        score = 0;
    },

    level3: function(){
        game.state.start('level3');
        score = 0;
    }
    
};