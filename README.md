# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
共三關，menu中可選擇，或者每打完一關，可以通往下一關。
技能在遊戲中的左下角，當技能值滿後，會有YOYO效果，此時可以按A鍵使用技能。
2. Animations : 
player有飛行中的動畫。
3. Particle Systems : 
當玩家或敵人受到攻擊後，會有噴血的效果。
4. Sound effects : 
攻擊和獲得分數音效

# Bonus Functions Description : 
1. Boss : 
第三關有大王，牠有三種攻擊模式：
a.射出扇形範圍子彈
b.扇形來回不斷射出子彈
c.追蹤彈
3. 子彈追蹤 : 
a.開了技能之後有子彈追蹤效果。
b.大王第三個攻擊模式。