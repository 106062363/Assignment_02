var menustate = {
    preload: function() {
        game.load.image('background', 'asset/sky.png');
        game.load.image('button1', 'asset/button1.png');
        game.load.image('button3', 'asset/button3.png');
    },
    create: function() {
        game.stage.backgroundColor = '#B045FF';
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'background');

        this.gamename = game.add.text(game.width/2, game.height/2-150, '天天打龍龍', { font: '100px DFKai-sb', fill: '#000' });
        this.gamename.anchor.setTo(0.5,0.5);

        this.button1 = game.add.button(game.width/2, game.height/2, 'button1', this.startgame, this, 2, 1, 0);
        this.button1.anchor.setTo(0.5,0.5);
        this.button1.scale.setTo(0.3, 0.3);

        this.button3 = game.add.button(game.width/2, game.height/2+150, 'button3', this.levelselect, this, 2, 1, 0);
        this.button3.anchor.setTo(0.5,0.5);
        this.button3.scale.setTo(0.3, 0.3);
    },

    update: function() {

    },

    startgame: function(){
        game.state.start('level1');
    },

    levelselect: function(){
        game.state.start('levelselect');
    }
};