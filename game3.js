var mainState3 = {
    preload: function () {
        game.load.spritesheet('enemy1', 'asset/boss.png', 165, 179);
        game.load.spritesheet('player', 'asset/dragon.png', 112, 108);
        game.load.image('background', 'asset/sky.png');
        game.load.image('bullet', 'asset/bullet.png');
        game.load.image('enemybullet', 'asset/enemybullet3.png');
        game.load.image('pixel', 'asset/pixel.png');
        game.load.image('skill', 'asset/skill.png');
        game.load.image('skillbullet', 'asset/skillbullet.png');

        game.load.audio('getcoin', 'asset/getcoin.mp3');
        game.load.audio('short', 'asset/short.mp3');
    },
    create: function () {
        level = 3;
        game.stage.backgroundColor = '#000000';
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'background');

        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple('15', 'bullet');
        this.bullets.setAll('scale.x', 0.7);
        this.bullets.setAll('scale.y', 0.7);
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 0.5);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

        game.physics.arcade.enable(this.bullets);

        this.skillbullet = game.add.group();
        this.skillbullet.enableBody = true;
        this.skillbullet.createMultiple('30', 'skillbullet');
        this.skillbullet.setAll('scale.x', 1.5);
        this.skillbullet.setAll('scale.y', 1.5);
        this.skillbullet.setAll('anchor.x', 0.5);
        this.skillbullet.setAll('anchor.y', 0.5);
        this.skillbullet.setAll('outOfBoundsKill', true);
        this.skillbullet.setAll('checkWorldBounds', true);

        game.physics.arcade.enable(this.skillbullet);

        this.bulletdelay = 150;
        this.shotdelay = 150;


        this.enemybullet = game.add.group();
        this.enemybullet.enableBody = true;
        this.enemybullet.createMultiple('300', 'enemybullet');
        this.enemybullet.setAll('scale.x', 1);
        this.enemybullet.setAll('scale.y', 1);
        this.enemybullet.setAll('anchor.x', 0.5);
        this.enemybullet.setAll('anchor.y', 0.5);
        this.enemybullet.setAll('outOfBoundsKill', true);
        this.enemybullet.setAll('checkWorldBounds', true);

        game.physics.arcade.enable(this.enemybullet);
        this.enemybulletdelay = 300;
        this.enemyshotdelay = 800;


        this.enemy1 = game.add.group();
        this.enemy1.enableBody = true;
        this.enemy1.createMultiple('1', 'enemy1');
        this.enemy1.setAll('scale.x', 1);
        this.enemy1.setAll('scale.y', 1);
        this.enemy1.setAll('anchor.x', 0.5);
        this.enemy1.setAll('anchor.y', 0.5);
        this.enemy1.setAll('outOfBoundsKill', true);
        this.enemy1.setAll('checkWorldBounds', true);
        this.state = 1;
        this.angle = -250;
        this.anglerange = 50;

        game.physics.arcade.enable(this.enemy1);
        this.enemydelay = 500;
        this.enexistdelay = 1000;
        this.enemynum = 0;

        this.player = game.add.sprite(game.width / 2, game.height - 100, 'player');
        this.player.physicsBodyType = Phaser.Physics.ARCADE;
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.scale.setTo(1, 1);
        this.player.body.setSize(10,10,(this.player.width-10)/2,(this.player.height-10)/2);
        this.player.animations.add('dragonmove', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 20, true);
        this.player.blood = 100;

        this.scoreString = 'Score : ';
        this.scoretext = game.add.text(10, 10, this.scoreString + score, {
            font: '50px Microsoft JhengHei',
            fill: '#000'
        });

        this.hpString = 'HP : ';
        this.hpText = game.add.text(game.width - 210, 10, this.hpString + this.player.blood, {
            font: '50px Arial',
            fill: '#000'
        });


        this.skill = game.add.sprite(60, game.height - 70, 'skill'); //
        this.skill.scale.setTo(0.3, 0.3);
        this.skill.anchor.setTo(0.5, 0.5);
        this.skill.alpha = 0.2;
        this.skill.yoyoing = 0;

        this.useskill = 0;



        //this.emitter.gravity = 500;

        control = game.input.keyboard.createCursorKeys();
        this.keys = game.input.keyboard; //

        this.getcoinsound = game.add.audio('getcoin');
        this.shortsound = game.add.audio('short');
    },

    update: function () {
        this.player.body.collideWorldBounds = true;
        this.starfield.tilePosition.y += 3;
        this.player.animations.play('dragonmove');

        this.movePlayer();
        this.bulletexist();
        this.enemybulletexist();
        this.enemyexist();

        game.physics.arcade.overlap(this.enemy1, this.skillbullet, this.hitenmey, null, this);
        game.physics.arcade.overlap(this.enemy1, this.bullets, this.hitenmey, null, this);
        game.physics.arcade.overlap(this.player, this.enemy1, this.hitplayer, null, this);
        game.physics.arcade.overlap(this.player, this.enemybullet, this.bullethitplayer, null, this);

    },
    endgame: function () {
        game.state.start('gameover');
    },

    hitenmey: function (enemy, bullet) {
        this.emitter = game.add.emitter(enemy.x, enemy.y, 50);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-200, -500);
        this.emitter.setXSpeed(-200, 200);
        this.emitter.start(true, 1000, null, 50);
        this.shortsound.play();
        bullet.kill();
        enemy.blood -= 5;

        if (this.skill.alpha <= 1) this.skill.alpha += 0.05;
        else {
            if (!this.skill.yoyoing) {
                game.add.tween(this.skill.scale).to({
                    x: 1,
                    y: 1
                }, 100).yoyo(true).start();
                this.skill.yoyoing = 1;
            }
        }

        if (enemy.blood <= 200) {enemy.tint = 0xff7d75; this.state =3}
        else if (enemy.blood <= 400) {enemy.tint = 0xffd8d6; this.state=2}

        if (enemy.blood <= 0) {
            enemy.kill();
            score += 200;
            this.scoretext.text = this.scoreString + score;
            this.getcoinsound.play();
        }
    },

    hitplayer: function (player, enemy) {
        player.blood -= 20;
        enemy.blood -= 10;

        if (enemy.blood <= 200) {enemy.tint = 0xff7d75; this.state =3}
        else if (enemy.blood <= 400) {enemy.tint = 0xffd8d6; this.state=2}

        player.y += 100;
        player.x += 70 * game.rnd.integerInRange(-1, 1);
        this.hpText.text = this.hpString + player.blood;

        this.emitter = game.add.emitter(player.x, player.y, 50);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(200, 500);
        this.emitter.setXSpeed(-200, 200);
        this.emitter.start(true, 1000, null, 50);

        if (player.blood <= 0) {
            win = 0;
            this.endgame();
        }
    },

    bullethitplayer: function (player, enemybullet) {
        this.emitter = game.add.emitter(player.x, player.y, 50);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(200, 500);
        this.emitter.setXSpeed(-200, 200);
        this.emitter.start(true, 1000, null, 50);
        enemybullet.kill();
        player.blood -= 10;
        this.hpText.text = this.hpString + player.blood;

        if (player.blood <= 30) player.tint = 0xff7d75;
        else if (player.blood <= 70) player.tint = 0xffd8d6;

        if (player.blood <= 0) {
            win = 0;
            this.endgame();
        }

    },

    bulletexist: function () {
        if (game.time.now > this.bulletdelay) {
            if (!this.useskill) {
                if (this.bullets) {
                    var bullet = this.bullets.getFirstExists(false);
                    bullet.reset(this.player.x, this.player.y - 30);
                    bullet.body.velocity.y = -500;
                    this.bulletdelay = game.time.now + this.shotdelay;
                }
            }
            else{
                if(this.skillbullet){
                    var liveenemy = [];
                    liveenemy.length = 0;
                    this.enemy1.forEachAlive(function (live) {
                        liveenemy.push(live);
                    });
                    if (liveenemy.length > 0) {
                        var rndenemy = game.rnd.integerInRange(0, liveenemy.length - 1);
                        var theenemy = liveenemy[rndenemy];
                        var bullet = this.skillbullet.getFirstExists(false);
                        bullet.reset(this.player.x, this.player.y - 30);
                        bullet.body.velocity.y = theenemy.y - this.player.y;
                        bullet.body.velocity.x = theenemy.x - this.player.x;
                        this.bulletdelay = game.time.now + 100;
                    }
                }
            }
        }
    },

    enemybulletexist: function(){
        if(game.time.now > this.enemybulletdelay){
            var liveenemy =[];
            liveenemy.length = 0;
            this.enemy1.forEachAlive(function(live){
                liveenemy.push(live);
            });

            if(liveenemy.length>0){
                var rndenemy = game.rnd.integerInRange(0,liveenemy.length-1);
                var theenemy = liveenemy[rndenemy];
                
                if(this.state == 1 ){
                    var randomx = game.rnd.integerInRange(-200,-100);
                    for(var i =0 ; i<10; i++){
                        var enemybullet = this.enemybullet.getFirstExists(false);
                        enemybullet.reset(theenemy.x,theenemy.y+30);
                        enemybullet.body.velocity.x = randomx + i*30;
                        enemybullet.body.velocity.y = 150;
                        this.enemybulletdelay = game.time.now + 500;
                    }
                }
                else if(this.state ==2 ){
                    var enemybullet = this.enemybullet.getFirstExists(false);
                    enemybullet.reset(theenemy.x,theenemy.y+30);
                    enemybullet.body.velocity.x = this.angle + this.anglerange;
                    enemybullet.body.velocity.y = 250;
                    this.angle+=this.anglerange;
                    if(this.angle>=250) this.anglerange = game.rnd.integerInRange(-60,-40);
                    else if(this.angle<=-250) this.anglerange = game.rnd.integerInRange(40,60);

                    this.enemybulletdelay = game.time.now + 50;
                }

                else if(this.state == 3){
                    var enemybullet = this.enemybullet.getFirstExists(false);
                    enemybullet.reset(theenemy.x,theenemy.y+30);
                    enemybullet.body.velocity.x = (this.player.x - theenemy.x)*1;
                    enemybullet.body.velocity.y = (this.player.y - theenemy.y)*1;

                    this.enemybulletdelay = game.time.now + 100;
                }
                
            }
        }

    },

    enemyexist: function () {
        if (game.time.now > this.enemydelay && this.enemynum < 1) {
            var enemy = this.enemy1.getFirstExists(false);
            enemy.reset(game.width / 2, 100);
            // enemy.body.velocity.y = game.rnd.integerInRange(70,100);
            enemy.blood = 600;
            enemy.tint = 0xffffff;
            this.enemydelay = game.time.now + this.enexistdelay;
            this.enemynum++;
            enemy.body.collideWorldBounds = true;
        }
        if (this.enemy1.countDead() == 1 && this.enemynum == 1) {
            level++;
            win = 1;
            this.endgame();
        }
    },

    movePlayer: function () {
        if (control.left.isDown && control.up.isDown) {
            this.player.x -= 7;
            this.player.y -= 7;
        } else if (control.right.isDown && control.up.isDown) {
            this.player.x += 7;
            this.player.y -= 7;
        } else if (control.left.isDown && control.down.isDown) {
            this.player.x -= 7;
            this.player.y += 7;
        } else if (control.right.isDown && control.down.isDown) {
            this.player.x += 7;
            this.player.y += 7;
        } else if (control.left.isDown) {
            this.player.x -= 7;

        } else if (control.right.isDown) {
            this.player.x += 7;

        } else if (control.up.isDown) {
            this.player.y -= 7;

        } else if (control.down.isDown) {
            this.player.y += 7;

        }

        if (this.keys.isDown(65) && this.skill.alpha >= 1) { //
            this.useskill = 1;
            setTimeout(()=>{
                this.useskill = 0;
                this.skill.alpha = 0.2;
                this.skill.yoyoing = 0;
            },2000);
        }
    }
};